package com.mandiri.latihancrud.service;

import com.mandiri.latihancrud.model.entity.Customer;
import com.mandiri.latihancrud.model.request.CreateCustomerRequest;
import com.mandiri.latihancrud.repository.CustomerRepository;
import com.mandiri.latihancrud.util.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PostCustomerService {
    private final CustomerRepository customerRepository;

    public Customer execute (CreateCustomerRequest customer){
        Customer newCustomer = Customer.builder()
                .email(customer.getEmail())
                .address(customer.getAddress())
                .fullName(customer.getFullName())
                .birthDate(customer.getBirthDate())
                .build();

        Optional <Customer> customerOpt = customerRepository.findByEmail(customer.getEmail());

        if (customerOpt.isPresent()){
            throw new NotFoundException("Email already exist");
        }

        newCustomer.setCreatedAt(LocalDateTime.now());
        newCustomer.setCreatedBy(customer.getFullName());
        newCustomer.setActiveStatus(true);

        return customerRepository.save(newCustomer);
    }


}
