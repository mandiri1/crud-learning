package com.mandiri.latihancrud.repository;

import com.mandiri.latihancrud.model.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

    Optional<Customer> findByEmail (String email);

    Optional<Customer> findByIdAndActiveStatusIsTrue (String id);

//    @Query(value = "INSERT INTO customer (full_name, birth_date, email, address, status)" +
//            "VALUES (:fullName, :birtDate, :email, :address, :status)", nativeQuery = true)
//    public Customer createCustomer(String fullName, String birthDate, String email, String address, String status );
}
