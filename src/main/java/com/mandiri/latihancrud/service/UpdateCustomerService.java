package com.mandiri.latihancrud.service;

import com.mandiri.latihancrud.model.entity.Customer;
import com.mandiri.latihancrud.model.request.CreateCustomerRequest;
import com.mandiri.latihancrud.model.request.UpdateCustomerRequest;
import com.mandiri.latihancrud.repository.CustomerRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class UpdateCustomerService {
    private final CustomerRepository customerRepository;

    public UpdateCustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer execute (UpdateCustomerRequest customer){

        Optional<Customer> customerOpt = customerRepository.findById(customer.getId());

        if (customerOpt.isEmpty()){
            throw new RuntimeException("id not found!");
        }

        Customer newCustomer = customerOpt.get();

        var fullName = newCustomer.getFullName();
        var email = newCustomer.getEmail();
        var address = newCustomer.getAddress();
        var birthDate = newCustomer.getBirthDate();
        
        if (StringUtils.isNotBlank(customer.getFullName())) {
            fullName = customer.getFullName();
        }
        
        if (StringUtils.isNotBlank(customer.getAddress())){
            address = customer.getAddress();
        }

        if (customer.getBirthDate() != null){
            birthDate = customer.getBirthDate();
        }

        if (StringUtils.isNotBlank(customer.getEmail())) {
            email = customer.getEmail();
        }

        var customerUpdate = Customer.builder().fullName(fullName)
                .id(newCustomer.getId())
                .address(address)
                .birthDate(birthDate)
                .email(email)
                .createdAt(newCustomer.getCreatedAt())
                .createdBy(newCustomer.getCreatedBy())
                .activeStatus(newCustomer.getActiveStatus())
                .build();

        return customerRepository.save(customerUpdate);
    }




//     if (StringUtils.isNotBlank(customer.getFullName())) {
//        fullName = customer.getFullName();
//    }
//
//        if (StringUtils.isNotBlank(newCustomer.getAddress()) && StringUtils.isNotBlank(customer.getAddress())){
//        address = customer.getAddress();
//    } else if (StringUtils.isNotBlank(customer.getAddress())) {
//        address = customer.getAddress();
//    }
//
//        if (newCustomer.getBirthDate() != null && customer.getBirthDate() != null){
//        birthDate = customer.getBirthDate();
//    } else if (StringUtils.isNotBlank((CharSequence) customer.getBirthDate())) {
//        birthDate = customer.getBirthDate();
//    }
//
//        if (StringUtils.isNotBlank(customer.getEmail())) {
//        email = customer.getEmail();
//    }
}
