package com.mandiri.latihancrud.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mandiri.latihancrud.model.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCustomerRequest {

    private String fullName;

    private String address;

    private String email;

    private Date birthDate;


}
