package com.mandiri.latihancrud.util;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String errMessage){
        super(errMessage);
    }
}
