package com.mandiri.latihancrud.service;

import com.mandiri.latihancrud.model.entity.Customer;
import com.mandiri.latihancrud.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteCustomerService {

    private final CustomerRepository customerRepository;

//    public DeleteCustomerService(CustomerRepository customerRepository) {
//        this.customerRepository = customerRepository;
//    }

    public void execute (String id){
        Customer customer = customerRepository.findById(id).get();
        customer.setActiveStatus(false);
    customerRepository.save(customer);
    }
}
