package com.mandiri.latihancrud.controller;


import com.mandiri.latihancrud.model.entity.Customer;
import com.mandiri.latihancrud.model.request.CreateCustomerRequest;
import com.mandiri.latihancrud.model.request.UpdateCustomerRequest;
import com.mandiri.latihancrud.model.response.Response;
import com.mandiri.latihancrud.service.DeleteCustomerService;
import com.mandiri.latihancrud.service.GetCustomerService;
import com.mandiri.latihancrud.service.PostCustomerService;
import com.mandiri.latihancrud.service.UpdateCustomerService;
import com.mandiri.latihancrud.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
//@RequiredArgsConstructor
public class CustomerController {
    PostCustomerService postCustomerService;
    GetCustomerService getCustomerService;
    UpdateCustomerService updateCustomerService;

    DeleteCustomerService deleteCustomerService;

    @Autowired
    public CustomerController(PostCustomerService postCustomerService, GetCustomerService getCustomerService, UpdateCustomerService updateCustomerService, DeleteCustomerService deleteCustomerService) {
        this.postCustomerService = postCustomerService;
        this.getCustomerService = getCustomerService;
        this.updateCustomerService = updateCustomerService;
        this.deleteCustomerService = deleteCustomerService;
    }

    @PostMapping
    public ResponseEntity<Response<Customer>> saveCustomer(@RequestBody CreateCustomerRequest customers){
        Response<Customer> response = new Response<Customer>();
        response.setData(postCustomerService.execute(CreateCustomerRequest.builder()
                .fullName(customers.getFullName())
                .email(customers.getEmail())
                .address(customers.getAddress())
                .birthDate(customers.getBirthDate())
                .build()));
        response.setMessage("New Customer inserted successfully");

        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                        .body(response);

    }

    @GetMapping("/getall")
    public List<Customer> getAllCustomer(){
        return getCustomerService.execute();
    }

    @GetMapping("/getById")
    public Customer getCustomerById(@RequestParam String id) throws NotFoundException {
        return getCustomerService.executeById(id);
    }

    @PutMapping
    public ResponseEntity<Response<Customer>> updateCustomer(@RequestBody UpdateCustomerRequest customer){
        Response<Customer> response = new Response<Customer>();
        response.setMessage("Customer successfully update");
        response.setData(updateCustomerService.execute(customer));
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @DeleteMapping("/delete")
    public void deleteCustomer(@RequestParam String id){
        deleteCustomerService.execute(id);
    }

//    @DeleteMapping("/{id}")
//    public ResponseEntity<Response<Customer>> deletedCustomer(@PathVariable String id){
//        System.out.println(id);
//        Response<Customer> response = new Response<Customer>();
//        response.setMessage("Customer successfully deleted");
//        deleteCustomerService.execute(id);
//        return ResponseEntity.status(HttpStatus.ACCEPTED)
//                .contentType(MediaType.APPLICATION_JSON)
//                .body(response);
//    }
}
