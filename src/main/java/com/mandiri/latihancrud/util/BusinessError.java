package com.mandiri.latihancrud.util;

public class BusinessError {
    public static final String ERR_NOT_FOUND = "User not found or has been deleted";
}
