package com.mandiri.latihancrud.service;

import com.mandiri.latihancrud.model.entity.Customer;
import com.mandiri.latihancrud.repository.CustomerRepository;
import com.mandiri.latihancrud.util.BusinessError;
import com.mandiri.latihancrud.util.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GetCustomerService {

    private final CustomerRepository customerRepository;

    public GetCustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> execute (){
        return customerRepository.findAll();
    }

    public Customer executeById (String id) throws NotFoundException {
            Optional<Customer> customerOpt = customerRepository.findByIdAndActiveStatusIsTrue(id);
            if (customerOpt.isEmpty()) {
                throw new NotFoundException(BusinessError.ERR_NOT_FOUND);
                //harusnya balikin null/data kosong/error 404/kurang tepat
            }
        return customerOpt.get();
    }


}
