package com.mandiri.latihancrud.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Table(name = "mst_customer")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Customer {
    @Id
    @Column(name = "customer_id")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @Column(name = "full_name",nullable = false)
    private String fullName;

    @Column(name = "address")
    private String address;
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "birth_date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "UTC")
    private Date birthDate;

    @Column(name = "created_at")
    //@CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "update_at")
    @UpdateTimestamp
    private LocalDateTime updateAt;

    //private String updateBy;

//    @Column(name = "status")
//    private String status;

    @Column(name = "active_status")
    private Boolean activeStatus;
}
